import {addX} from "./modules/add-x.js";
import {addY} from "./modules/add-y.js";

const sum = addX() + addY();

console.log(sum);